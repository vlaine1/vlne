import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';
import Heading from '@theme/Heading';

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header>
      <div className="container">
        <Heading as="h1" className="hero__title">
          {siteConfig.title}
        </Heading>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            Documentations
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home(): JSX.Element {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <HomepageHeader />
      <main>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
          <div style={{ flex: 1, margin: '0 0.5rem', textAlign: 'center' }}>
            <img src="/assets/devops.svg" alt="DevOps" />
          </div>
          <div style={{ flex: 1, margin: '0 0.5rem', textAlign: 'center' }}>
            <img src="/assets/security.svg" alt="Security" />
          </div>
          <div style={{ flex: 1, margin: '0 0.5rem', textAlign: 'center' }}>
            <img src="/assets/computing.svg" alt="Computing" />
          </div>
        </div>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            Documentations
          </Link>
        </div>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}